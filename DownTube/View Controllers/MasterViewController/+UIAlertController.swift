//
//  +UIAlertController.swift
//  DownTube
//
//  Created by Betim S on 3/24/18.
//  Copyright © 2018 bettimms. All rights reserved.
//

import UIKit
import SafariServices
import Alamofire

extension MasterViewController{
    /// Builds and shows a UIAlertController for the user to decide if they want to enter link or browse for a video
    ///
    /// - Parameters:
    ///   - enterLinkAction: action that takes place if user wants to enter link
    ///   - browseAction: action that takes place if user wants to browse
    public func buildAndShowAlertControllerForNewVideo(enterLinkAction: @escaping (UIAlertAction) -> Void, browseAction: @escaping (UIAlertAction) -> Void
        , getVideoUrls: @escaping (UIAlertAction) -> Void) {
        let alertVC = UIAlertController(title: "How do you want to find the video?", message: nil, preferredStyle: UIDevice.current.userInterfaceIdiom == .phone ? .actionSheet : .alert)
        
        alertVC.addAction(UIAlertAction(title: "Enter Link", style: .default, handler: enterLinkAction))
        alertVC.addAction(UIAlertAction(title: "Browse", style: .default, handler: browseAction))
        alertVC.addAction(UIAlertAction(title: "By list", style: .default, handler: getVideoUrls))
        alertVC.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alertVC, animated: true, completion: nil)        
    }
   
    func getVideoUrls(){
        let pageSize = 10
        let apiKey = "AIzaSyAq8vXekAXzNMdxhFGpj9iBFu88Mh_Snzw"
        let playlistId = "PLx3Fh1kiMbrdyyW5an82OWMJkp8sCX7Dz" //97 videos
        //let playlistId = "PLx3Fh1kiMbrcOUYqEcLnNSmxVhIeKQexC" //211 videos
        var url = URL(string: "https://www.googleapis.com/youtube/v3/playlistItems?playlistId=\(playlistId)&maxResults=\(pageSize)&part=snippet%2CcontentDetails&key=\(apiKey)")!
        
        var prevToken:String?
         var pageCounter = 0
        func recursive(token: String,result:Int)->Void {
            url = URL(string: "https://www.googleapis.com/youtube/v3/playlistItems?playlistId=\(playlistId)&pageToken=\(token)&maxResults=\(pageSize)&part=snippet%2CcontentDetails&key=\(apiKey)")!
            if(pageCounter < result){
                pageCounter += pageSize
//                print(pageCounter)
                self.getPage(url: url, completion: recursive)
            }
        }
        getPage(url: url, completion:recursive)
    }
    
    func getPage(url:URL, completion:@escaping (String,Int)->Void){
        let baseUrl = "https://www.youtube.com/watch?v="
        Alamofire.request(url).responseJSON { (response) in
            
            guard let JSON = response.result.value,
                let dictionary = JSON as? [String: Any],
                let items = dictionary["items"] as? [AnyObject],
                let pageInfo = dictionary["pageInfo"] as? AnyObject,
                let nextPageToken = dictionary["nextPageToken"] as? String
                else { return }
            let totalResults = pageInfo["totalResults"] as! Int
            print(nextPageToken)
            let videos: [String] = items.map {
                //                print($0)
                let videoID = $0.value(forKeyPath: "snippet.resourceId.videoId") as! String
                let videoTitle = $0.value(forKeyPath: "snippet.title") as! String
                let videoDescription = $0.value(forKeyPath: "snippet.description") as! String
                let videoThumbnailUrl = $0.value(forKeyPath: "snippet.thumbnails.high.url") as! String
                self.startDownloadOfVideoInfoFor("\(baseUrl+videoID)")
//                    print(videoTitle)
                return videoID
            }
            
            completion(nextPageToken,totalResults)
        }
    }
    /**
     Presents a UIAlertController that gets youtube video URL from user then streams the video
     
     - parameter sender: button
     */
    
    
    /// Opens up a safari VC and lets the user browse for the video. Shows them how to do so if they haven't before
    ///
    /// - Parameter keepReference: keep a reference to the safari vc in self.presentedSafariVC
    public func browseForVideoAction(keepReference: Bool) {
        if UserDefaults.standard.bool(forKey: Constants.shownSafariDialog) {
            
            //User has been shown dialog, just show it
            let vc = self.showSafariVC()
            if keepReference { self.presentedSafariVC = vc }
            
        } else {
            
            //Tell user how to use the safari view controller and then proceed
            UserDefaults.standard.set(true, forKey: Constants.shownSafariDialog)
            let alertVC = UIAlertController(title: "How to Use", message: "Once you've found a video you want to add, hit the share button and select the \"Add to DownTube\" action on the top row.", preferredStyle: .alert)
            alertVC.addAction(UIAlertAction(title: "Got it", style: .default) { [unowned self] _ in
                let vc = self.showSafariVC()
                if keepReference { self.presentedSafariVC = vc }
            })
            self.present(alertVC, animated: true, completion: nil)
        }
    }
    
    /// Shows an SFSafariViewController. Assumes url is valid
    func showSafariVC(with urlString: String = "https://youtube.com") -> SFSafariViewController {
        let vc = SFSafariViewController(url: URL(string: urlString)!)
        vc.preferredBarTintColor = .black
        vc.preferredControlTintColor = .white
        self.present(vc, animated: true, completion: nil)
        return vc
    }
    
    /**
     Presents a UIAlertController that gets youtube video URL from user, calls completion if successful. Shows error otherwise.
     
     - parameter actionName: title of the AlertController. "<actionName> YouTube Video". Either "Download" or "Stream"
     - parameter completion: code that is called once the user hits "OK." Closure parameter is the text gotten from user
     */
    func buildAndShowUrlGettingAlertController(_ actionName: String, completion: @escaping (String) -> Void) {
        let alertController = UIAlertController(title: "\(actionName) YouTube Video", message: "Video will be shown in 720p or the highest available quality", preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "Ok", style: .default) { _ in
            let textField = alertController.textFields![0]
            
            if let text = textField.text {
                if text.count > 10 {
                    completion(text)
                } else {
                    self.showErrorAlertControllerWithMessage("URL too short to be valid")
                }
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addTextField() { textField in
            textField.placeholder = "Enter YouTube video URL"
            textField.keyboardType = .URL
            textField.becomeFirstResponder()
            textField.inputAccessoryView = self.buildAccessoryButton()
        }
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    /**
     Builds the button that is the input accessory view that is above the keyboard
     
     - returns:  button for accessory keyboard view
     */
    // swift
    func buildAccessoryButton() -> UIView {
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 40))
        button.setTitle("Paste from clipboard", for: UIControlState())
        button.backgroundColor = #colorLiteral(red: 0.5882352941, green: 0.5882352941, blue: 0.5882352941, alpha: 1)
        button.setTitleColor(#colorLiteral(red: 0.2941176471, green: 0.2941176471, blue: 0.2941176471, alpha: 1), for: .highlighted)
        button.addTarget(self, action: #selector(self.pasteFromClipboard), for: .touchUpInside)
        
        return button
    }
    
    /**
     Pastes the text from the clipboard in the showing alert vc, if it exists
     */
    @objc
    func pasteFromClipboard() {
        if let alertVC = self.presentedViewController as? UIAlertController {
            alertVC.textFields![0].text = UIPasteboard.general.string
        }
    }
    
    
    
    // MARK: - Helper methods
    
    /**
     Presents UIAlertController error message to user with ok button
     
     - parameter message: message to show
     */
    func showErrorAlertControllerWithMessage(_ message: String?) {
        let alertController = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        if let safariVC = self.presentedViewController as? SFSafariViewController {
            //Safari VC is presented, show it there
            safariVC.present(alertController, animated: true, completion: nil)
        } else {
            //Show on this VC
            self.present(alertController, animated: true, completion: nil)
        }
    }
}
