//
//  +VideoOptions.swift
//  DownTube
//
//  Created by Betim S on 3/24/18.
//  Copyright © 2018 bettimms. All rights reserved.
//

import UIKit

extension MasterViewController{
    
    /// Plays a video in an AVPlayer and handles all callbacks
    ///
    /// - Parameters:
    ///   - url: url of the video, local or remote
    ///   - video: video object (either streaming or downloaded) that is being played
    ///   - progressCallback: called to update the video watch state, called multiple time
    public func playVideo(with url: URL, video: Watchable, progressCallback: @escaping (WatchState) -> Void) {
        let playerObjects = self.createPlaybackViewController(with: url, video: video)
        
        self.present(videoViewController: playerObjects.viewController, andSetUpNowPlayingInfoFor: playerObjects.player, video: video, progressCallback: progressCallback)
    }
    
    /**
     Handles long touching on a cell. Can mark cell as watched or unwatched
     
     - parameter gestureRecognizer: gesture recognizer
     */
    @objc
    func handleLongTouchWithGestureRecognizer(_ gestureRecognizer: UILongPressGestureRecognizer) {
        if gestureRecognizer.state == .changed || gestureRecognizer.state == .ended {
            
            let point = gestureRecognizer.location(in: self.tableView)
            guard let indexPath = self.tableView.indexPathForRow(at: point) else {
                return
            }
            
            let video = PersistentVideoStore.shared.fetchedVideosController.object(at: indexPath)
            
            let alertController = UIAlertController(title: nil, message: nil, preferredStyle: UIDevice.current.userInterfaceIdiom == .phone ? .actionSheet : .alert)
            
            for action in self.buildActionsForLongPressOn(video: video, at: indexPath) {
                alertController.addAction(action)
            }
            
            alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            
            self.present(alertController, animated: true, completion: nil)
        }        
    }
    
    /**
     Builds the alert actions for when a user long presses on a cell
     
     - parameter video:     video to build the actions for
     - parameter indexPath: location of the cell
     
     - returns: array of actions
     */
    func buildActionsForLongPressOn(video: Video, at indexPath: IndexPath) -> [UIAlertAction] {
        var actions: [UIAlertAction] = []
        var video = video
        
        //If the user progress isn't nil, that means that the video is unwatched or partially watched
        if video.watchProgress != .watched {
            actions.append(UIAlertAction(title: "Mark as Watched", style: .default) { [unowned self] _ in
                video.watchProgress = .watched
                PersistentVideoStore.shared.save()
                self.tableView.reloadRows(at: [indexPath], with: .none)
            })
        }
        
        //If the user progress isn't 0, the video is either partially watched or done
        if video.watchProgress != .unwatched {
            actions.append(UIAlertAction(title: "Mark as Unwatched", style: .default) { [unowned self] _ in
                video.watchProgress = .unwatched
                PersistentVideoStore.shared.save()
                self.tableView.reloadRows(at: [indexPath], with: .none)
            })
        }
        
        //Opening in youtube
        if let youtubeUrl = video.youtubeUrl {
            actions.append(UIAlertAction(title: "Open in YouTube", style: .default) { [unowned self] _ in
                self.presentedSafariVC = self.showSafariVC(with: youtubeUrl)
            })
        }
        
        //Sharing the video
        if let streamUrl = video.streamUrl, let localUrl = self.videoManager.localFilePathForUrl(streamUrl) {
            
            if let localPath = self.videoManager.localFileLocationForUrl(streamUrl) {
                actions.append(UIAlertAction(title: "Edit Video", style: .default) { [unowned self] _ in
                    let editor = UIVideoEditorController()
                    editor.delegate = self.videoEditingHandler
                    editor.videoPath = localPath
                    editor.videoMaximumDuration = 0
                    editor.videoQuality = .typeIFrame1280x720
                    
                    self.videoManager.currentlyEditingVideo = video
                    
                    self.present(editor, animated: true, completion: nil)
                })
            }
            
            actions.append(UIAlertAction(title: "Share", style: .default) { [unowned self] _ in
                let activityViewController = UIActivityViewController(activityItems: [localUrl], applicationActivities: nil)
                self.present(activityViewController, animated: true, completion: nil)
            })
        }
        return actions
    }
}
