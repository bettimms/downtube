//
//  +AppInfo.swift
//  DownTube
//
//  Created by Betim S on 3/24/18.
//  Copyright © 2018 bettimms. All rights reserved.
//

import Foundation
extension MasterViewController{
    /**
     Shows the "About this App" view controller
     
     - parameter sender: button that sent the action
     */
    @objc
    func showAppInfo(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "ShowAppInfo", sender: self)
    }    
}
