//
//  +VideoManagerDelegate.swift
//  DownTube
//
//  Created by Betim S on 3/24/18.
//  Copyright © 2018 bettimms. All rights reserved.
//

import Foundation

// MARK: - VideoManagerDelegate

extension MasterViewController: VideoManagerDelegate {
    
    func reloadRows(_ rows: [IndexPath]) {
        DispatchQueue.main.async() {
            self.tableView.reloadRows(at: rows, with: .none)
        }
    }
    
    func updateDownloadProgress(_ download: Download, at index: Int, with totalSize: String) {
        DispatchQueue.main.async() {
            if let videoTableViewCell = self.tableView.cellForRow(at: IndexPath(row: index, section: 0)) as? VideoTableViewCell {
                
                videoTableViewCell.updateProgress(for: download, totalSize: totalSize)
                if download.isDone {
                    let indexPath = IndexPath(row: index, section: 0)
                    self.tableView.reloadRows(at: [indexPath], with: .automatic)
                }
            }
        }
    }
}
