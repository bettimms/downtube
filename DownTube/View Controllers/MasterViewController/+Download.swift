//
//  +Download.swift
//  DownTube
//
//  Created by Betim S on 3/24/18.
//  Copyright © 2018 bettimms. All rights reserved.
//

import UIKit
import  XCDYouTubeKit

extension MasterViewController{
    /**
     Presents a UIAlertController that gets youtube video URL from user then downloads the video
     
     - parameter sender: button
     */
    @IBAction func startDownloadingVideoAction(_ sender: AnyObject) {
        self.buildAndShowAlertControllerForNewVideo(enterLinkAction: { [unowned self] _ in
            self.buildAndShowUrlGettingAlertController("Download") { text in
                self.startDownloadOfVideoInfoFor(text)
            }
            }, browseAction: { [unowned self] _ in
                self.browseForVideoAction(keepReference: false)
            }, getVideoUrls: {_ in
                self.getVideoUrls()
        })
    }
    
    /**
     Creates the entity and cell from provided URL, starts download
     
     - parameter url: stream URL for video
     */
    func startDownloadOfVideoInfoFor(_ url: String) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        //Gets the video id, which is the last 11 characters of the string
        XCDYouTubeClient.default().getVideoWithIdentifier(String(url.suffix(11))) { video, error in
            self.videoManager.videoObject(video, downloadedForVideoAt: url, error: error as NSError?)
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
    }
    /// Checks if any videos downloaded in background, and if they are, reloads the rows
    @objc
    func checkIfVideosDownloadedInBackground() {
        let indexPaths = self.videoManager.checkIfAnyVideosDownloadedSuccessfully()
        
        self.tableView.reloadRows(at: indexPaths, with: .none)
    }
    /**
     Plays video in fullscreen player
     
     - parameter video:     video that is going to be played
     - parameter indexPath: index path of the video
     */
    func playDownload(_ video: Video, atIndexPath indexPath: IndexPath) {
        var video = video
        if let urlString = video.streamUrl, let url = self.videoManager.localFilePathForUrl(urlString) {
            self.playVideo(with: url, video: video) { [weak self] newProgress in
                
                video.watchProgress = newProgress
                PersistentVideoStore.shared.save()
                
                self?.editVideoIndexPathToReload = indexPath
            }
        }
    }
}
