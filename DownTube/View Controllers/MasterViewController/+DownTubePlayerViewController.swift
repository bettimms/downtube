//
//  +DownTubePlayerViewController.swift
//  DownTube
//
//  Created by Betim S on 3/24/18.
//  Copyright © 2018 bettimms. All rights reserved.
//

import XCDYouTubeKit

extension MasterViewController{
    /// Creates a DownTubePlayerViewController with the url to play from and the video that contains the watch progress
    ///
    /// - Parameters:
    ///   - url: url that the VC will play from
    ///   - video: video that is playing
    /// - Returns: set up DownTubePlayerViewController
    public func createPlaybackViewController(with url: URL, video: Watchable) -> (player: AVPlayer, viewController: DownTubePlayerViewController) {
        let player = AVPlayer(url: url)
        
        let playerViewController = DownTubePlayerViewController()
        playerViewController.updatesNowPlayingInfoCenter = false
        playerViewController.currentlyPlaying = video
        playerViewController.actionItemsDelegate = self
        
        //Seek to time if the time is saved
        switch video.watchProgress {
        case let .partiallyWatched(seconds):
            player.seek(to: CMTime(seconds: seconds.doubleValue, preferredTimescale: 1))
        default:    break
        }
        
        playerViewController.player = player        
        return (player, playerViewController)
    }
    
    /// Presents the DownTubePlayerViewController and sets up the NowPlayingInfoCenter for playback
    ///
    /// - Parameters:
    ///   - player: player set up with video
    ///   - viewController: DownTubePlayerViewController to display
    ///   - video: video that's being played
    ///   - progressCallback: callback for when the progress of the video is updated
    public func present(videoViewController: DownTubePlayerViewController, andSetUpNowPlayingInfoFor player: AVPlayer, video: Watchable, progressCallback: @escaping (WatchState) -> Void) {
        self.present(videoViewController, animated: true) {
            player.play()
            //This sets the name in control center and on the home screen
            self.nowPlayingHandler = NowPlayingHandler(player: player)
            self.nowPlayingHandler?.addTimeObserverToPlayer(progressCallback)
            MPNowPlayingInfoCenter.default().nowPlayingInfo = video.nowPlayingInfo
            MPNowPlayingInfoCenter.default().nowPlayingInfo?[MPNowPlayingInfoPropertyPlaybackRate] = 1
            MPNowPlayingInfoCenter.default().nowPlayingInfo?[MPMediaItemPropertyPlaybackDuration] = NSNumber(value: CMTimeGetSeconds(player.currentItem!.duration))
        }
    }
}
