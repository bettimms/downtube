//
//  +Stream.swift
//  DownTube
//
//  Created by Betim S on 3/24/18.
//  Copyright © 2018 bettimms. All rights reserved.
//

import UIKit

extension MasterViewController{
    @IBAction func startStreamingVideoAction(_ sender: AnyObject) {
        self.buildAndShowAlertControllerForNewVideo(enterLinkAction: { [unowned self] _ in
            self.buildAndShowUrlGettingAlertController("Stream") { text in
                self.startStreamOfVideoInfoFor(text)
            }
            }, browseAction: { [unowned self] _ in
                self.streamFromSafariVC = true
                self.browseForVideoAction(keepReference: true)
            }, getVideoUrls: {_ in
                self.getVideoUrls()
        })
    }
    /// Starts the stream of a video
    ///
    /// - Parameter youTubeUrl: youtube url for the video
    func startStreamOfVideoInfoFor(_ youTubeUrl: String) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        self.videoManager.getStreamInfo(for: youTubeUrl) { [unowned self] url, streamingVideo, error in
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            guard let url = url, var streamingVideo = streamingVideo, error == nil else {
                self.showErrorAlertControllerWithMessage(error!.localizedDescription)
                return
            }
            
            //Now that a video has either been found in core data or created, get the watch progress and send it to the AVPlayer
            self.playVideo(with: url, video: streamingVideo) { newProgress in
                streamingVideo.watchProgress = newProgress
                PersistentVideoStore.shared.save()
            }
        }
    }
}
