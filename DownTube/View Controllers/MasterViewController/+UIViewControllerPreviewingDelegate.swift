//
//  +UIViewControllerPreviewingDelegate.swift
//  DownTube
//
//  Created by Betim S on 3/24/18.
//  Copyright © 2018 bettimms. All rights reserved.
//

import UIKit

// MARK: 3D Touch

extension MasterViewController: UIViewControllerPreviewingDelegate {
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        guard let indexPath = self.tableView.indexPathForRow(at: location) else { return nil }
        
        //Get the index path for the cell
        let video = PersistentVideoStore.shared.fetchedVideosController.object(at: indexPath)
        if let indexPath = self.tableView.indexPathForRow(at: location), let urlString = video.streamUrl, let url = self.videoManager.localFilePathForUrl(urlString) {
            //This will show the blur correctly
            let rectOfCellInTableView = self.tableView.rectForRow(at: indexPath)
            let rectOfCellInSuperview = self.tableView.convert(rectOfCellInTableView, to: self.view)
            previewingContext.sourceRect = rectOfCellInSuperview
            return self.createPlaybackViewController(with: url, video: video).viewController
        }
        
        return nil
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        if let vc = viewControllerToCommit as? DownTubePlayerViewController, let player = vc.player, var video = vc.currentlyPlaying {
            self.present(videoViewController: vc, andSetUpNowPlayingInfoFor: player, video: video) { newProgress in
                video.watchProgress = newProgress
                PersistentVideoStore.shared.save()
            }
        }
    }
    
}
