//
//  MasterViewController.swift
//  DownTube
//
//  Created by Adam Boyd on 2016-05-30.
//  Copyright © 2016 Adam. All rights reserved.
//

import UIKit
import CoreData
import AVKit
import AVFoundation
import XCDYouTubeKit
import MMWormhole
import SafariServices

public let normalCellRowHeight: CGFloat = 57
public let downloadingCellRowHeight: CGFloat = 92

class MasterViewController: UITableViewController, VideoEditingHandlerDelegate, NSFetchedResultsControllerDelegate {
    
    let wormhole = MMWormhole(applicationGroupIdentifier: "group.bettimms.DownTube", optionalDirectory: nil)
    
    var videoManager: VideoManager!
    var fileManager: FileManager = .default
    var editVideoIndexPathToReload: IndexPath? //Update the watch and download status from other VCs/background download
    let videoEditingHandler = VideoEditingHandler()
    var streamFromSafariVC = false
    weak var presentedSafariVC: SFSafariViewController?
    var nowPlayingHandler: NowPlayingHandler?
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 11.0, *) {
            //Setting up the nav bar for iOS 11, with large titles and search
            self.navigationController?.navigationBar.prefersLargeTitles = true
            self.navigationItem.largeTitleDisplayMode = .always
            
            let search = UISearchController(searchResultsController: nil)
            search.searchResultsUpdater = self
            search.searchBar.tintColor = .red
            self.navigationItem.searchController = search
            
            UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = [NSAttributedStringKey.foregroundColor.rawValue: UIColor.white]

        }
        
        self.navigationItem.leftBarButtonItem = self.editButtonItem

        let infoButton = UIBarButtonItem(title: "About", style: .plain, target: self, action: #selector(self.showAppInfo(_:)))
        self.navigationItem.rightBarButtonItem = infoButton
        
        PersistentVideoStore.shared.fetchedVideosController.delegate = self
        
        self.videoEditingHandler.delegate = self
        self.videoManager = VideoManager(delegate: self, fileManager: self.fileManager)
        
        self.videoManager.addVideosFromSharedArray()
        
        self.registerForPreviewing(with: self, sourceView: self.tableView)
        
        //Wormhole between extension and app
        self.wormhole.listenForMessage(withIdentifier: "youTubeUrl") { messageObject in
            if let vc = self.presentedSafariVC, let url = messageObject as? String, self.streamFromSafariVC {
                //If the user wants to stream the video, dismiss the safari vc and stream
                vc.dismiss(animated: true) {
                    self.startStreamOfVideoInfoFor(url)
                }
                self.presentedSafariVC = nil
            } else {
                //Else, add the video to the download queue
                self.videoManager.messageWasReceivedFromExtension(messageObject)
            }
        }
        
        // Deletes any files that shouldn't be there
        DispatchQueue.global(qos: .background).async {
            self.videoManager.cleanUpDownloadedFiles(from: PersistentVideoStore.shared)
            self.videoManager.checkIfAnyVideosNeedToBeDownloaded()
        }
        
        //Need to initialize so no error when trying to save to them
        _ = PersistentVideoStore.shared.fetchedVideosController
        _ = PersistentVideoStore.shared.fetchedStreamingVideosController
        
        //Videos downloaded in background
        self.checkIfVideosDownloadedInBackground()
        NotificationCenter.default.addObserver(self, selector: #selector(self.checkIfVideosDownloadedInBackground), name: Notification.Name.UIApplicationDidBecomeActive, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //Whenever the view controller appears, no media items are playing
        self.nowPlayingHandler = nil
        
        //If video edited
        if let indexPath = self.editVideoIndexPathToReload {
            self.tableView.reloadRows(at: [indexPath], with: .automatic)
            self.editVideoIndexPathToReload = nil
        }
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}










