//
//  +DownTubePlayerViewControllerDelegate.swift
//  DownTube
//
//  Created by Betim S on 3/24/18.
//  Copyright © 2018 bettimms. All rights reserved.
//

import Foundation
// MARK: - DownTubePlayerViewControllerDelegate

extension MasterViewController: DownTubePlayerViewControllerDelegate {
    
    func playerViewController(_ playerViewController: DownTubePlayerViewController, requestsToOpenVideoUrlInYouTube urlString: String) {
        self.presentedSafariVC = self.showSafariVC(with: urlString)
    }
    
    func viewControllerChangedVideoStatus(for video: Watchable?) {
        self.tableView.reloadData()
    }
}
