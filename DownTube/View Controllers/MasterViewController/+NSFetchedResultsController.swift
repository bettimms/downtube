//
//  +NSFetchedResultsController.swift
//  DownTube
//
//  Created by Betim S on 3/24/18.
//  Copyright © 2018 bettimms. All rights reserved.
//

import UIKit
import CoreData
import AVKit
import AVFoundation
import XCDYouTubeKit
import MMWormhole
import SafariServices

extension MasterViewController{
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        switch type {
        case .insert:       self.tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:       self.tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        default:            return
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:       tableView.insertRows(at: [newIndexPath!], with: .fade)
        case .delete:       tableView.deleteRows(at: [indexPath!], with: .fade)
        case .update:       tableView.reloadRows(at: [indexPath!], with: .automatic)
        case .move:         tableView.moveRow(at: indexPath!, to: newIndexPath!)
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.endUpdates()
    }
}
