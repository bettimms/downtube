//
//  +VideoTableViewCellDelegate.swift
//  DownTube
//
//  Created by Betim S on 3/24/18.
//  Copyright © 2018 bettimms. All rights reserved.
//

import Foundation

// MARK: VideoTableViewCellDelegate

extension MasterViewController: VideoTableViewCellDelegate {
    func pauseTapped(_ cell: VideoTableViewCell) {
        if let indexPath = self.tableView.indexPath(for: cell) {
            let video = PersistentVideoStore.shared.fetchedVideosController.object(at: indexPath)
            self.videoManager.downloadManager.pauseDownload(video)
            self.tableView.reloadRows(at: [indexPath], with: .none)
        }
    }
    
    func resumeTapped(_ cell: VideoTableViewCell) {
        if let indexPath = self.tableView.indexPath(for: cell) {
            let video = PersistentVideoStore.shared.fetchedVideosController.object(at: indexPath)
            self.videoManager.downloadManager.resumeVideoDownload(video)
            self.tableView.reloadRows(at: [indexPath], with: .none)
        }
    }
    
    func cancelTapped(_ cell: VideoTableViewCell) {
        if let indexPath = tableView.indexPath(for: cell) {
            let video = PersistentVideoStore.shared.fetchedVideosController.object(at: indexPath)
            self.videoManager.downloadManager.cancelDownload(video)
            tableView.reloadRows(at: [indexPath], with: .none)
            self.videoManager.deleteVideoObject(at: indexPath)
        }
    }
}
