//
//  +UISearchResultsUpdating.swift
//  DownTube
//
//  Created by Betim S on 3/24/18.
//  Copyright © 2018 bettimms. All rights reserved.
//

import UIKit

extension MasterViewController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        guard let search = searchController.searchBar.text else { return }
        
        print("Updating search results, search is \"\(search)\"")
        PersistentVideoStore.shared.setSearchForDownloadedVideos(search, isDownloaded: nil)
        self.tableView.reloadData()
    }
}
