//
//  +TableView.swift
//  DownTube
//
//  Created by Betim S on 3/24/18.
//  Copyright © 2018 bettimms. All rights reserved.
//

import UIKit

extension MasterViewController{
    override func numberOfSections(in tableView: UITableView) -> Int {
        return PersistentVideoStore.shared.fetchedVideosController.sections?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionInfo = PersistentVideoStore.shared.fetchedVideosController.sections![section]
        return sectionInfo.numberOfObjects
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VideoTableViewCell", for: indexPath) as! VideoTableViewCell
        let video = PersistentVideoStore.shared.fetchedVideosController.object(at: indexPath)
        var download: Download?
        if let streamUrl = video.streamUrl, let downloadingVideo = self.videoManager.downloadManager.getDownloadWith(streamUrl: streamUrl) {
            download = downloadingVideo
        }
        
        let holdGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongTouchWithGestureRecognizer(_:)))
        holdGestureRecognizer.minimumPressDuration = 1
        cell.addGestureRecognizer(holdGestureRecognizer)
        
        cell.setUp(with: video, download: download, isDownloaded: self.videoManager.localFileExistsFor(video), delegate: self)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let video = PersistentVideoStore.shared.fetchedVideosController.object(at: indexPath)
        if self.videoManager.localFileExistsFor(video) {
            self.playDownload(video, atIndexPath: indexPath)
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let video = PersistentVideoStore.shared.fetchedVideosController.object(at: indexPath)
        return (video.isDoneDownloading?.boolValue ?? true) ? normalCellRowHeight : downloadingCellRowHeight
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            _ = self.videoManager.deleteDownloadedVideo(at: indexPath)
            
            self.videoManager.deleteVideoObject(at: indexPath)
        }
    }
}
